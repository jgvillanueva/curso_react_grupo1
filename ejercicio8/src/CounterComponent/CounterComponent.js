import React, {Component} from "react";
import {connect} from 'react-redux';
import {decrement, increment} from "../actions/counterActions";

class CounterComponent extends Component {
    render() {
        return (
            <div className="counterComponent">
                <button
                    onClick={this.props.increment}
                >Incrementar</button>
                <button
                    onClick={this.props.decrement}
                >Reducir</button>
                <h1>Contador: {this.props.count}</h1>
            </div>
        );
    }
}

const mapStateToProps = state => ({
    count: state.counterReducer.count,
});

const mapDispatchToProps = (dispatch) => {
    return {
        increment: () => {
            dispatch(increment());
        },
        decrement: () => {
            dispatch(decrement());
        },
    }
}


export default connect(mapStateToProps, mapDispatchToProps)(CounterComponent);
