import React from 'react';
import chai, {expect} from 'chai';
import chaiEnzyme from 'chai-enzyme';
import {configure, shallow} from 'enzyme';
import Adapter from 'enzyme-adapter-react-16';
import {act} from "react-dom/test-utils";
import configureMockStore from "redux-mock-store";

import App from "../App";
import thunk from "redux-thunk";
import {Provider} from "react-redux";
import * as types from "../actions/actionTypes";
import * as actions from '../actions/counterActions';
import {createStore} from "redux";
import reducer from "../reducers/counterReducer";

const mockStore = configureMockStore([thunk]);

configure({
    adapter: new Adapter()
})

let wrapper;
beforeEach(() => {
    const store = mockStore({
        count: 0
    })
    act(() => {
        wrapper = shallow(
            <Provider store={store}>
                <App/>
            </Provider>
            );
    })
})
afterEach(() => {
    wrapper.unmount();
})

describe('Redux tests', () => {
    describe('Testing actions', () => {
        it('Incrment change store to 1', () => {
            const action = {
                type: types.INCREMENT,
            }
            expect(actions.increment()).to.eql(action);
        })
    })

    describe('Testing reducers', () => {
        let store;
        store = createStore(reducer);
        it('Test nitial state', () => {
            expect(store.getState().count).to.equal(0);
        })
        it('Add one to the initial state', () => {
            store.dispatch(actions.increment());
            expect(store.getState().count).to.equal(1);
        })
        it('Remove one to the initial state', () => {
            store.dispatch(actions.decrement());
            expect(store.getState().count).to.equal(0);
        })
    })

    chai.use(chaiEnzyme());
});

