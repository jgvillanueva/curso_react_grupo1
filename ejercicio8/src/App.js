
import './App.css';
import {connect} from 'react-redux';
import CounterComponent from "./CounterComponent/CounterComponent";

function App() {
  return (
    <div className="App">
      <CounterComponent/>
    </div>
  );
}

export default connect()(App);
