import React from 'react';
import PropTypes from 'prop-types';

class ErrorComponent extends React.Component {
    constructor(props) {
        super(props);

    }

    render(){
        let error
        if(this.props.errorText !== '' && this.props.touched) {
            error = <div className="alert alert-danger my-3">
                {this.props.errorText}
            </div>
        }
        return (
            <div className="error">
                {error}
            </div>
        )
    }
}
export default ErrorComponent;

const customValidation = (props, propName, componentName) => {
    if(props[propName]) {
        let value = props[propName];
        if(value.length < 10) {
            return new Error (`${propName} en ${componentName} debe tener más de 10 caracteres`);
        }
        return null;
    }
    return new Error (`${propName} en ${componentName} es obligatoria`);
}

ErrorComponent.propTypes = {
    //errorText: PropTypes.string.isRequired,
    errorText: customValidation,
    touched: PropTypes.bool.isRequired,
}
/*
ErrorComponent.defaultProps = {
    errorText: 'Sin error definido',
    touched: true,
}
*/
