import React from 'react';
import Error from "../Error/ErrorComponent";
import {useFormik} from 'formik';

const FormUncontrolled = () => {
    const validate = (values) => {
        let errors = {};
        if(values.asunto === '') {
            errors.asunto = 'Hace falta asunto';
        } else if(values.asunto.length < 5) {
            errors.asunto = 'El asunto es corto';
        } else {
            errors.asunto = '';
        }
        if(values.mensaje === '') {
            errors.mensaje = 'Hace falta mensaje';
        } else {
            errors.mensaje = '';
        }
        if(errors.asunto === '' && errors.mensaje === '') {
            errors = null;
        }
        return errors;
    }

    const formik = useFormik({
        initialValues: {
            asunto: '',
            mensaje: '',
        },
        validate,
        onSubmit: (values) => {
            console.log('los valores son', values);
        }
    });


    return (
        <div className="card">
            <div className="card-header">
                <h1>Form UnControlled</h1>
            </div>
            <div className="card-body">
                <form onSubmit={formik.handleSubmit} >
                    <div className="form-group">
                        <label htmlFor="asunto">Asunto</label>
                        <input
                            className="form-control"
                            name="asunto"
                            type="text"
                            onChange={formik.handleChange}
                            value={formik.values.asunto}
                        />
                        {
                            formik.errors.asunto ?
                                <div className="alert alert-danger my-3">{formik.errors.asunto}</div>
                                    :
                                    null
                        }
                    </div>
                    <div className="form-group">
                        <label htmlFor="mensaje">Asunto</label>
                        <textarea
                            className="form-control"
                            name="mensaje"
                            onChange={formik.handleChange}
                            value={formik.values.mensaje}
                        ></textarea>
                        {
                            formik.errors.mensaje ?
                                <div className="alert alert-danger my-3">{formik.errors.mensaje}</div>
                                :
                                null
                        }
                    </div>
                    <button type="submit" className="btn btn-primary"
                            disabled={!(formik.isValid && formik.dirty)}
                    >Enviar</button>

                </form>
            </div>
        </div>
    )
}
export default FormUncontrolled;



/*
import React from 'react';
import Error from "../Error/Error";
class FormUncontrolled extends React.Component {
    constructor(props) {
        super(props);

        this.asuntoInput = React.createRef();
        this.mensajeInput = React.createRef();

        this.handlSubmit = this.handlSubmit.bind(this);
    }

    handlSubmit(event) {
        event.preventDefault();
        console.log('Los datos enviados son',
            this.asuntoInput.current.value,
            this.mensajeInput.current.value
        );
    }

    render(){
        return (
            <div className="card">
                <div className="card-header">
                    <h1>Form UnControlled</h1>
                </div>
                <div className="card-body">
                    <form  onSubmit={this.handlSubmit}>
                        <div className="form-group">
                            <label htmlFor="asunto">Asunto</label>
                            <input
                                className="form-control"
                                name="asunto"
                                type="text"
                                ref={this.asuntoInput}
                            />
                        </div>
                        <div className="form-group">
                            <label htmlFor="mensaje">Asunto</label>
                            <textarea
                                className="form-control"
                                name="mensaje"
                                ref={this.mensajeInput}
                            ></textarea>
                        </div>
                        <button type="submit" className="btn btn-primary">Enviar</button>
                    </form>
                </div>
            </div>
        )
    }
}
export default FormUncontrolled;
*/
