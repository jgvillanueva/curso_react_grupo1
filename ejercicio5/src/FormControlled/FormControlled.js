import React from 'react';
import ErrorComponent from "../Error/ErrorComponent";
class FormControlled extends React.Component {
    constructor(props) {
        super(props);

        this.state = {
            mensaje: {
                asunto: '',
                mensaje: '',
            },
            formErrors: {
                asunto: 'El asunto es necesario',
                mensaje: 'El mensaje es necesario',
            },
            formTouched: {
                asunto: false,
                mensaje: false,
            },
            formValid: false
        }
        this.handlSubmit = this.handlSubmit.bind(this);
        this.handleChange = this.handleChange.bind(this);
        this.handleBlur = this.handleBlur.bind(this);
    }


    handlSubmit(event) {
        event.preventDefault();
        console.log('Los datos enviados son', this.state.mensaje);
    }

    handleBlur(event) {
        const formTouched = this.state.formTouched;
        formTouched[event.target.name] = true;
        this.setState({
            formTouched,
        })
    }

    handleChange(event) {
        const {mensaje} = this.state;
        mensaje[event.target.name] = event.target.value;
        this.setState({
            mensaje,
        }, () => {
            this.validateForm(event.target.name, event.target.value)
        })
    }

    validateForm(fieldName, value) {
        let fieldValidationErrors = this.state.formErrors;
        let message;
        let valid
        switch (fieldName) {
            case 'asunto':
                valid = value && value.length > 5;
                message = valid ? '' : 'el asunto hace falta y es muy corto';
            case 'mensaje':
                valid = value && value.length > 12;
                message = valid ? '' : 'El mensaje hace falta y es muy corto';
            default:

        }
        fieldValidationErrors[fieldName] = message;
        this.setState({
            formErrors: fieldValidationErrors,
        }, this.validateState)
    }

    validateState() {
        let fieldValidationErrors = this.state.formErrors;
        let formValid = true;
        Object.keys(fieldValidationErrors).forEach((field) => {
            if(fieldValidationErrors[field] !== '') {
                formValid = false;
            }
        })
        this.setState({
            formValid
        })
    }

    render(){
        return (
            <div className="card">
                <div className="card-header">
                    <h1>Form Controlled</h1>
                </div>
                <div className="card-body">
                    <form onSubmit={this.handlSubmit}>
                        <div className="form-group">
                            <label htmlFor="asunto">Asunto</label>
                            <input
                                className="form-control"
                                name="asunto"
                                type="text"
                                value={this.state.mensaje.asunto}
                                onChange={this.handleChange}
                                onBlur={this.handleBlur}
                            />
                            <ErrorComponent
                                   touched={this.state.formTouched.asunto} />
                        </div>
                        <div className="form-group">
                            <label htmlFor="mensaje">Asunto</label>
                            <textarea
                                className="form-control"
                                name="mensaje"
                                value={this.state.mensaje.mensaje}
                                onChange={this.handleChange}
                                onBlur={this.handleBlur}
                            ></textarea>
                            <ErrorComponent errorText={this.state.formErrors.mensaje}
                                   touched={this.state.formTouched.mensaje} />
                        </div>
                        <button className="btn btn-primary" disabled={!this.state.formValid}
                        >Enviar</button>
                    </form>
                </div>
            </div>
        )
    }
}
export default FormControlled;
