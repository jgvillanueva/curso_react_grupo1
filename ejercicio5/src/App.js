import React, {useState} from 'react';

import './App.css';
import Toggle from "./Toggle/Toggle";
import FormControlled from "./FormControlled/FormControlled";
import FormUncontrolled from "./FormUncontrolled/FormUncontrolled";
import 'bootstrap/dist/css/bootstrap.css';

function App() {
    const [tipo, setTipo] = useState(1);


    const toggleHandler = (value) => {
        console.log('Se ha hecho click con la opción', value);
        setTipo(value);
    }

    let form;
    if (tipo === 1) {
        form = <FormControlled />
    } else {
        form = <FormUncontrolled />
    }

  return (
    <div className="App">
      <Toggle
          label1="Controlled"
          label2="UnControlled"
          clickHandler={toggleHandler}
          initialValue={tipo}
      />
        { form }
    </div>
  );
}

export default App;
