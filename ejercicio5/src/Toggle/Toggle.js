import React from 'react';
import './Toggle.css';

class Toggle extends React.Component {
    constructor(props) {
        super(props);

        this.state = {
            opcion: this.props.initialValue,
        }
    }

    clickHandler(value){
        this.setState({
            opcion: value,
        })
        this.props.clickHandler(value);
    }

    render(){
        const claseOption = 'option';
        return (
            <div className="toggle">
                <div
                    className={
                        this.state.opcion === 1 ? 'active ' + claseOption : 'disabled ' + claseOption
                    }
                    onClick={this.clickHandler.bind(this, 1)}
                >{this.props.label1}</div>
                <div
                    className={
                        this.state.opcion === 2 ? 'active ' + claseOption : 'disabled ' + claseOption
                    }
                    onClick={this.clickHandler.bind(this, 2)}
                >{this.props.label2}</div>
            </div>
        )
    }
}
export default Toggle;
