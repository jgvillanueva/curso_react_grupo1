import React from 'react';
import Item from "../Item/Item";
import './List.css';

class List extends React.Component {

    constructor(props) {
        super(props);
    }

    render() {
        const  {data} = this.props;
        const items = data.map((item) => {
            return <Item data={item} key={item.id} name={item.name} />
        });
        
        return (
            <ul className="list">
                {this.props.children}
                {items}
            </ul>
        )
    }
}

export default List;
