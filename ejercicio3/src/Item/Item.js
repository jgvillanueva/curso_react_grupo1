import React from 'react';
import './Item.css';

class Item extends React.Component {

    constructor(props) {
        super(props);

        this.state = {
            ampliado: false,
        };
    }

    toggleAmpliado() {
        const nuevoAmpliado = !this.state.ampliado;
        this.setState(
            {
                ampliado: nuevoAmpliado
            }
        );
    }

    render() {
        const claseAmpliado = this.state.ampliado ? 'item ampliado' : 'item';

        return (
            <li className={claseAmpliado} onClick={this.toggleAmpliado.bind(this)} >
                <span>{this.props.name}</span>
                {
                    this.state.ampliado &&
                        <div>El identificador es {this.props.data.id}</div>
                }
            </li>
        )
    }
}

export default Item;
