import React from 'react';
import List from "../List/List";
import Toggle from "../Toggle/Toggle";

class Container extends React.Component {
    elementos = [
        {id: 1, name: 'Ana'},
        {id: 2, name: 'Juan'},
        {id: 3, name: 'Carmen'},
        {id: 4, name: 'Jose'},
        {id: 5, name: 'Pablo'},
    ]

    constructor(props) {
        super(props);

        this.state = {
            mostrarLista: true,
        }

        this.handleToggle = this.handleToggle.bind(this);
    }

    handleToggle(mostrarLista) {
        this.setState({
            mostrarLista
        })
    }

     render() {
         return (
             <div>
                 <Toggle  handleToggle={this.handleToggle}/>
                 {
                     this.state.mostrarLista &&
                     (
                         <List data={this.elementos} >
                             <h1>Lista de usuarios</h1>
                         </List>
                     )
                 }

             </div>
         )
     }
}

export default Container;
