import React from 'react';

class Toggle extends React.Component {
    TEXTO_MOSTRAR = 'Mostrar Lista';
    TEXTO_OCULTAR = 'Ocultar Lista';

    constructor(props) {
        super(props);

        this.state = {
            mostrar: true
        }

        this.toggleTexto = this.toggleTexto.bind(this);
    }

    toggleTexto() {
        const mostrar = !this.state.mostrar;
        this.setState({
            mostrar
        })
        this.props.handleToggle(mostrar);
    }

    render() {
        return (
            <button className="toggle" onClick={this.toggleTexto}>
                {
                    this.state.mostrar ?
                        this.TEXTO_OCULTAR : this.TEXTO_MOSTRAR
                }
            </button>
        )
    }
}

export default Toggle;
