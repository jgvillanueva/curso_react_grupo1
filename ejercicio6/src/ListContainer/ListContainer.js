import React from 'react';
import List from "../List/List";
import {
    BrowserRouter as Router,
    Switch,
    Route,
    Link,
    Redirect,
} from "react-router-dom";
import Detail from "../Detail/Detail";

class ListContainer extends React.Component {
    constructor(props) {
        super(props);

    }

    render(){

        return (
            <div className="listContainer">
                <h1>Lista de mensajes</h1>
                <Router>
                    <Switch>
                        <Route exact path="/list" component={List}  />
                        <Route path="/list/:id" component={Detail} />
                    </Switch>
                </Router>
            </div>
        )
    }
}
export default ListContainer;
