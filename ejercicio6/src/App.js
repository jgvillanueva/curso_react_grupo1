import logo from './logo.svg';
import './App.css';
import Container from "./Container/Container";
import 'bootstrap/dist/css/bootstrap.min.css';

function App() {
  return (
    <div className="App">
      <Container />
    </div>
  );
}

export default App;
