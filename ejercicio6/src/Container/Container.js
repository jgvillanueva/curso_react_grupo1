import React from 'react';
import ListContainer from "../ListContainer/ListContainer";
import New from "../New/New";
import './container.scss';
import {
    BrowserRouter as Router,
    Switch,
    Route,
    Link,
    Redirect,
} from "react-router-dom";

class Container extends React.Component {
    constructor(props) {
        super(props);

    }

    render(){

        return (
            <Router >
                <div className="container">
                    <nav>
                        <ul>
                            <li><Link to='/'>Home</Link></li>
                            <li><Link to='/new'>New</Link></li>
                        </ul>
                    </nav>
                    <Switch>
                        <Route path='/' exact>
                            <Redirect to="/list" />
                        </Route>
                        <Route path="/list" exact>
                            <ListContainer />
                        </Route>
                        <Route path="/new">
                            <New />
                        </Route>
                    </Switch>
                </div>
            </Router>
        )
    }
}
export default Container;
