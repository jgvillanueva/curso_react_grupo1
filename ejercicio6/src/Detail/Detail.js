import React from 'react';
import {Link} from "react-router-dom";

class Detail extends React.Component {
    constructor(props) {
        super(props);

        this.state = {
            id: '',
            data: null
        }
    }

    componentDidMount() {
        const location = this.props.location;
        if(!location.state) {
            const { id } = this.props.match.params;
            this.setState({
                id
            })
        } else {
            const {data} = this.props.location.state;
            this.setState({
                data
            })
        }
    }

    handleClick() {
        this.props.history.goBack();
    }

    render(){
        let card
        if (this.state.data) {
            card = <div className='card'>
                <h1 className="card-header">{this.state.data.asunto}</h1>
                <div className="card-body">{this.state.data.mensaje}</div>
            </div>
        }
        return (
            <div className="listContainer">
                <h1>Detalle {this.state.id}</h1>
                <button className="btn btn-primary"
                        onClick={this.handleClick.bind(this)}
                >Volver</button>

                {card}
            </div>
        )
    }
}
export default Detail;
