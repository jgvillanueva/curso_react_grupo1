import React from 'react';
import './List.scss';
import {
    Link,
} from "react-router-dom";

class List extends React.Component {

    USER_ID = 13;

    constructor(props) {
        super(props);

        this.state = {
            mensajes: [],
        }

        this.getMensajes = this.getMensajes.bind(this);
    }

    componentDidMount() {
        const url = 'http://dev.contanimacion.com/api_tablon/api/mensajes';
        fetch(url, {method: 'GET'})
            .then(res => res.json())
            .then((result) => {
                console.log(result);
                this.setState({
                    mensajes: result
                })
            })
    }

    getClassCard(mensaje) {
        return this.USER_ID === mensaje.user_id ? 'card propio' : 'card';
    }

    getMensajes() {
        return(
            this.state.mensajes.map((mensaje) => {
                return <li key={mensaje.id} className={this.getClassCard(mensaje)}>
                    <div className="card-header">{mensaje.asunto}</div>
                    <div className="card-body">{mensaje.mensaje}</div>
                    <Link to={{
                        pathname: '/list/' + mensaje.id,
                        state: {
                            data: mensaje
                        }
                    }}>
                        <button className="btn btn-primary">Ver</button>
                    </Link>

                </li>
            })
        )
    }

    render(){
        return (
            <div className="list">
                <ul>
                    {this.getMensajes()}
                </ul>

            </div>
        )
    }
}
export default List;
