import React, {useEffect, useState} from 'react';
import UserDetail from "../UserDetail/UserDetail";

function UserList () {
    const [users, setUsers] = useState([]);
    const [user, setUser] = useState(null);
    const [error, setError] = useState(null);

    const getData = () => {
        const url = 'http://dev.contanimacion.com/api_tablon/api/users';
        const response = fetch(url, {
            method: 'GET'
        })
            .then(res => res.json())
            .then((result) => {
                console.log(result);
                setUsers(result);
                setError(null);
            })
            .catch((error) => {
                console.log(error)
                setError(error.message);
            })
    }

    useEffect(() => {
        getData();
    }, [])

    const selectUser = (_user) => {
        setUser(_user);
    }

    let userDOM;
    if (user) {
        userDOM = <UserDetail data={user}/>
    } else {
        userDOM = <div>No se ha seleccionado usuario</div>
    }

    if (error) {
        return (
            <div>Error cargando los datos: { error }</div>
        )
    } else {
        return (
            <div>
                <ul>
                    {
                        users.map((user) => {
                            return <li key={user.id} onClick={() => {
                                selectUser(user);
                            }}>{user.name}</li>
                        })
                    }
                </ul>
                {userDOM}
            </div>
        )
    }
}

export default UserList;
