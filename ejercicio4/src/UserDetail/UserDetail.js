import React, {useEffect, useState} from 'react';

function UserDetail (props) {
    const {data} = props;
    const [anteriores, setAnteriores] = useState([]);

    useEffect(() => {
        const _anteriores = anteriores;
        _anteriores.push(props.data.name);
        setAnteriores(_anteriores);
    }, [data]);

    return (
        <div>
            <div> Id: {props.data.id}</div>
            <div> Nombre: {props.data.name}</div>
            <div> email: {props.data.email}</div>

            {
                anteriores.length > 0 &&
                <div>Los anteriores son {anteriores.join(' ')}</div>
            }
        </div>
    )
}

export default UserDetail;
