import React, {useState} from 'react';

const Counter = () => {
    const [count, setCounter] = useState(0);

    const addOne = () => {
        setCounter(count + 1);
    }

    return(
        <div>
            <h1>{count}</h1>
            <h2 onClick={addOne}>Añadir a la cuenta</h2>
        </div>
    )
}

export default Counter;
