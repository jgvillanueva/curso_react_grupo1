import dispatcher from "../dispatcher";

export const COLOR_APP_ACTIONS = {
    CHANGE_COLOR: 'colorAppActions.changeColor',
}

export const changeColor = (color) => {
    console.log('Llega el color', color);
    dispatcher.dispatch({
        type: COLOR_APP_ACTIONS.CHANGE_COLOR,
        value: color,
    });
}
