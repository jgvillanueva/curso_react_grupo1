import React from 'react';
import * as ColorActions from "../actions/colorActions";
import colorStore from "../stores/colorStore";

class Botonera extends React.Component {
    constructor() {
        super();
        this.state = {
            colorTexto: colorStore.getactiveColor()
        };
        this.updateBackgroundcolor = this.updateBackgroundcolor.bind(this);
    }
    handleClick(value) {
        ColorActions.changeColor(value);
    }

    componentDidMount() {
        colorStore.on('colorStoreUpdated', this.updateBackgroundcolor);
    }

    componentWillUnmount() {
        colorStore.removeListener('colorStoreUpdated', this.updateBackgroundcolor);
    }

    updateBackgroundcolor() {
        this.setState({
            colorTexto: colorStore.getactiveColor()
        })
    }

    render(){
        return(
            <div className="botonera">
                <h1
                    style={{
                        color: this.state.colorTexto
                    }}
                >Botonera</h1>
                <nav>
                    <ul>
                        <li onClick={this.handleClick.bind(this, '#ff0000')} >rojo</li>
                        <li onClick={this.handleClick.bind(this, '#00ff00')} >verde</li>
                        <li onClick={this.handleClick.bind(this, '#0000ff')} >azul</li>
                    </ul>
                </nav>
            </div>
        )
    }
}

export default Botonera;
