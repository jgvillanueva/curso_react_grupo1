import React from 'react';
import chai, {expect} from 'chai';
import chaiEnzyme from 'chai-enzyme';
import {configure, shallow} from 'enzyme';
import Adapter from 'enzyme-adapter-react-16';
import {act} from "react-dom/test-utils";

import App from "../App";
import Botonera from "../Botonera/Botonera";
import Caja from "../Caja/Caja";

configure({
    adapter: new Adapter()
})

let wrapper;
beforeEach(() => {
    act(() => {
        wrapper = shallow(<App/>);
    })
})
afterEach(() => {
    wrapper.unmount();
})

describe('Testing app component', () => {
    it('App renders h1 title', () => {
        const h1 = <h1>Flux app</h1>;
        expect(wrapper).to.contain(h1);
    })
    chai.use(chaiEnzyme());

    describe('App renders Caja and Botonera', () => {
        it('App renders Botonera', () => {
            const botonera = <Botonera />
            expect(wrapper).to.contain(botonera);
        })
        it('App renders caja', () => {
            const caja = <Caja />
            expect(wrapper).to.contain(caja);
        })
        chai.use(chaiEnzyme());
    })

    describe('Botonera behavior', () => {
        let botonera;
        let caja;
        beforeEach(() => {
            botonera = wrapper.find('Botonera');
            caja = wrapper.find('Caja');

        })

        it('Clicks on red button', () => {
            const button = botonera.dive().find('li').first();
            /*console.log('******************');
            console.log(button.debug());
            console.log('******************');*/
            act(() => {
                button.simulate('click');
            })
            const card = caja.dive().find('.cajaContenedor');
            expect(card).to.have.style('background-color', '#ff0000');
        })
        it('Clicks on green button', () => {
            const button = botonera.dive().find('ul').childAt(1);
            act(() => {
                button.simulate('click');
            })
            const card = caja.dive().find('.cajaContenedor');
            expect(card).to.have.style('background-color', '#00ff00');
        })
        chai.use(chaiEnzyme());
    })
})



