import React from 'react';
import chai, {expect} from 'chai';
import chaiEnzyme from 'chai-enzyme';
import {configure, shallow} from 'enzyme';
import Adapter from 'enzyme-adapter-react-16';
import {act} from "react-dom/test-utils";

import Botonera from "../Botonera/Botonera";

configure({
    adapter: new Adapter()
})

let wrapper;
beforeEach(() => {
    act(() => {
        wrapper = shallow(<Botonera/>);
    })
})
afterEach(() => {
    wrapper.unmount();
})

describe('Testing botonera component', () => {
    it('App renders 3 li tags', () => {
        const lis = wrapper.find('li');
        expect(lis.length).to.equal(3);
    })
    chai.use(chaiEnzyme());
})



