import React from 'react';
import './App.css';
import Botonera from "./Botonera/Botonera";
import Caja from "./Caja/Caja";

function App() {


  return (
    <div className="App">
        <h1>Flux app</h1>
      <Botonera />
      <Caja />
    </div>
  );
}

export default App;
