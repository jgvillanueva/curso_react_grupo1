import React from 'react';
import './caja.css';
import colorStore from "../stores/colorStore";
class Caja extends React.Component {
    constructor() {
        super();

        this.state = {
            color: colorStore.getactiveColor(),
        }

        this.updateBackgroundcolor = this.updateBackgroundcolor.bind(this);
    }

    updateBackgroundcolor() {
        this.setState({
            color: colorStore.getactiveColor(),
        })
    }

    componentDidMount() {
        colorStore.on('colorStoreUpdated', this.updateBackgroundcolor);
    }

    componentWillUnmount() {
        colorStore.removeListener('colorStoreUpdated', this.updateBackgroundcolor);
    }

    render(){
        return(
            <div className="caja">
                <h1>Caja</h1>
                <div
                    className="cajaContenedor"
                    style={{
                        backgroundColor: this.state.color
                    }}
                >
                    Este es el contenido de la caja
                    <h1>{this.state.color}</h1>
                </div>
            </div>
        )
    }
}

export default Caja;
