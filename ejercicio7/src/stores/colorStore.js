import dispatcher from '../dispatcher';
import * as ColorActions from '../actions/colorActions';
import {EventEmitter} from 'events';

class ColorStore extends EventEmitter{
    activeColor = '#cccccc';

    handleActions(action) {
        switch(action.type) {
            case ColorActions.COLOR_APP_ACTIONS.CHANGE_COLOR:
                this.activeColor = action.value;
                this.emit('colorStoreUpdated');
            default:

        }
    }

    getactiveColor() {
        return this.activeColor;
    }
}

const colorStore = new ColorStore();
dispatcher.register(colorStore.handleActions.bind(colorStore));
export default colorStore;
